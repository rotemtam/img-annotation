(function() {
  'use strict';

  angular
    .module('imgAnnotate')
    .directive('annotationCanvas', annotationCanvas);

  /** @ngInject */
  function annotationCanvas() {
    var directive = {
      restrict: 'E',
      // scope: {
      //   img: '='
      // },
      templateUrl: 'app/components/annotation-canvas/canvas.html',
      controller: CanvasController,
      controllerAs: 'vm'
    };

    return directive;



    /** @ngInject */
    function CanvasController($log) {
      var vm = this;
      vm.annotations = [{
        x: 100, y: 100, type: 'car', height: 100, width: 100
      }];

      vm.onClick = function($event) {
        vm.annotations.push(createAnnotation($event.offsetX, $event.offsetY))
        $log.info(vm.annotations)
      };

      vm.onDragComplete = function($index, $data,$event) {
        $log.info($index, $data,$event)
        var annotation = vm.annotations[$index]
        annotation.x = $event.offsetX
        annotation.y = $event.offsetY
      };
    }

    function createAnnotation(x,y) {
      return {
        x: x,
        y: y,
        width: 100,
        height: 100,
        type: ''
      }
    }

  }



})();
