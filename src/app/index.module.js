(function() {
  'use strict';

  angular
    .module('imgAnnotate', ['ui.bootstrap', 'ngDraggable']);

})();
